#define SIZE 9
typedef struct stack{
	int arr[SIZE];
	int i;
}stack;
void init(stack *s);
void push(stack *s,int elem);
int pop(stack *s);
int isempty(stack *s);
int isfull(stack *s);