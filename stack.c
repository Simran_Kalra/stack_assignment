#include "stack.h"
int x;
void init(stack *s) {
	s->i=0;
}
void push(stack *s, int num) {
	s->arr[s->i++]=num;	
}
int pop(stack *s) {
	int temp;
	temp=s->arr[s->i-1];
	s->i--;
	return temp;
}
int isfull(stack *s){
	return s->i == SIZE;
}
int isempty(stack *s){
	return s->i == 0;
}
